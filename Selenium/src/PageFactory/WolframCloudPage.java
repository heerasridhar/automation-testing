package PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WolframCloudPage{
	
	WebDriver driver;	
	String result;
	
	@FindBy(xpath="/html/body/div[3]/div/div[1]/div/div[2]/div/div/div/div/div[4]/div/div[2]/div/div[3]/div/div[7]/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div")
	WebElement firstplaybutton;
	
	@FindBy(xpath="/html/body/div[3]/div/div[1]/div/div[2]/div/div/div/div/div[4]/div/div[2]/div/div[3]/div/div[10]/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div")    
	WebElement secondplaybutton;
	
	@FindBy(xpath="/html/body/div[3]/div/div[1]/div/div[2]/div/div/div/div/div[4]/div/div[2]/div/div[3]/div/div[9]/div/div[1]/div/div/div/div/span")
	WebElement output1;
	
	@FindBy(xpath="/html/body/div[3]/div/div[1]/div/div[2]/div/div/div/div/div[4]/div/div[2]/div/div[3]/div/div[12]/div/div[1]/div/div/div/div/span[2]")
	WebElement output2;
	
	@FindBy(xpath = "//span[contains(text(),'Fast Introduction to the Wolfram Language for Math')]")
	WebElement link;
	
	
	public WolframCloudPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public String FirstPlayButton() {
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(firstplaybutton)).click();
		return (new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(output1)).getText());
	}
	
	public String SecondPlayButton() {
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(secondplaybutton)).click();
		return (new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(output2)).getText());
	}
	
	public void ClickLink() {
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(link)).click();
		
	}
	
	public String Evaluate() throws InterruptedException {
		//this.ClickLink();
		//this.SecondPlayButton();
		return (this.FirstPlayButton());	
	}
	
	public String Evaluate1() throws InterruptedException {
		return (this.SecondPlayButton());
	}
	
}