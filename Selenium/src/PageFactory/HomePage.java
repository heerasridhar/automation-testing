package PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage{
	
	WebDriver driver;
	@FindBy(className="_3YyOB_vi")
	WebElement equalbtn;
	@FindBy(xpath = "/html/body/div/div/div/div/div/div[1]/section/form/div/div/input")
	WebElement textarea;
	
	@FindBy(xpath="/html/body/div/div/div/div/div/div[2]/ul[1]/li[1]/button/span/span")
	WebElement extendedKeyBoard;
	
	@FindBy(xpath="/html/body/div/div/div/div/div/div[3]/section/div/ul[1]/li[1]/button")
	WebElement pi;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void Operation(String numbers) {
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(textarea)).sendKeys(numbers);
	}
	
	public void clickOnEqual(){
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(equalbtn)).click();
		
	}
	
	public void clickOnKeyboard() {
		
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(extendedKeyBoard)).click();
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(pi)).click();
	}
	
	public void AdditionOperation(String numbers) {
		this.Operation(numbers);
		this.clickOnEqual();
	}
}