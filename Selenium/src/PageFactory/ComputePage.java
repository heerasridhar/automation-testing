package PageFactory;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ComputePage
{
	WebDriver driver;
	
	@FindBy(xpath="//h2[contains(text(),'Input:')]")
	WebElement Input;
	
	@FindBy(xpath = "//span[@class='_3ci9dP6l']//span[contains(text(),'Plain Text')]")
	WebElement text;
	
	@FindBy(xpath = "//span[contains(text(),'Continue in computable notebook �')]")
	WebElement notebook ;
	
	
   @FindBy(xpath="/html/body/div/div/div/main/div[3]/div/div[1]/section/section[1]/div[2]/ul/li[3]/button/span/span")
   WebElement newplaintext;
   
   @FindBy(xpath = "/html/body/div/div/div/main/div[3]/div[2]/div[1]/section/section[2]/div[2]/div/button/span")
	WebElement piElement;
   
	public ComputePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	

	public void clickOnPlainText() {
		new Actions(driver).moveToElement(new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Input:')]")))).perform();
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(text)).click();
	}
	
	public void newplaintext() {
		new Actions(driver).moveToElement(new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div/div/main/div[3]/div[2]/div[1]/section/section[1]/header/h2")))).perform();
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(newplaintext)).click();
	}
    public void clickOnNotebook() {   	
    	new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(notebook)).click();
    }
    
    public String getpitext() {
		
		return (new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(piElement)).getText());
	}
    
    public void HoverActions() {		
		this.clickOnPlainText();
		this.clickOnNotebook();
	}
	
}