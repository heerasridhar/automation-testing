package FunctionalTest;

import org.testng.annotations.Test;
import org.testng.Assert;
import PageFactory.HomePage;
import PageFactory.WolframCloudPage;
import PageFactory.ComputePage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class TestCases {

	WebDriver driver;
	String baseUrl = "http://www.wolframalpha.com/";
	String solution = null;
	String solution2 = null;
	String solution3 = null;
	String pivariable="Pi";
	String expectedurl="https://www.wolfram.com/language/fast-introduction-for-math-students/en/";
	HomePage obj1;
	ComputePage obj2;
	WolframCloudPage obj3;
	
	
	@BeforeTest
	@Parameters("browser")
	public void CrossBrowserTesting(String browser) throws Exception{
		//Check if parameter passed from TestNG is 'firefox'
		if(browser.equalsIgnoreCase("firefox")){
		//create firefox instance
			System.setProperty("webdriver.gecko.driver","C:\\Users\\heera\\Downloads\\geckodriver-v0.26.0-win64\\geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
		}
		//Check if parameter passed as 'chrome'
		else if(browser.equalsIgnoreCase("chrome")){
			//set path to chromedriver.exe
			System.setProperty("webdriver.chrome.driver","C:\\Users\\heera\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
			//create chrome instance
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}
		
		else {
			throw new Exception ("Browser not supported");
		}	
	}	
	
	@Test(priority = 0)
	
	public void computeValues() throws InterruptedException {
		
		driver.get(baseUrl);
		
		obj1 = new HomePage(driver);
		obj2 = new ComputePage(driver);
		obj3 = new WolframCloudPage(driver);		
		String expectedResult = "4";
		String expectedResult1 = "four";
		obj1.AdditionOperation("2+2");
		obj2.HoverActions();		
		solution = obj3.Evaluate();	
		
		try {
			//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Assert.assertTrue(expectedResult.equalsIgnoreCase(solution));
		}
		catch(Throwable e) {
			e.printStackTrace();
		}
		
		solution2 = obj3.Evaluate1();
		
		try {
		    //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Assert.assertTrue(expectedResult1.equalsIgnoreCase(solution2));
		}
		catch(Throwable e) {
			e.printStackTrace();
		}
		
	}
	
	@Test(priority = 1)
	public void clickNewLink() throws InterruptedException {		
		obj3.ClickLink();
	}	
	
	
	@Test(priority=2)
	
	public void keyboard() throws InterruptedException{
		
		driver.get(baseUrl);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		obj1 = new HomePage(driver);
		obj2 = new ComputePage(driver);
		
		obj1.clickOnKeyboard();
		obj1.clickOnEqual();
	
		obj2.newplaintext();
	   solution3= obj2.getpitext();
	 
		
		try {
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Assert.assertTrue(pivariable.equalsIgnoreCase(solution3));
		}
		catch(Throwable e) {
			e.printStackTrace();
		}
		
	}
	
	
	@AfterTest 
	public void closeBrowser() { 
		driver.quit(); 
	}
		
}
