package newpackage;

import java.util.Set;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;

public class MyClass {

    public static void main(String[] args)  {
    	//System.setProperty("webdriver.chrome.driver","C:\\Users\\heera\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
    	System.setProperty("webdriver.gecko.driver","C:\\Users\\heera\\Downloads\\geckodriver-v0.26.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		String baseUrl = "https://www.wolframalpha.com/";
       
        // launch Fire fox and direct it to the Base URL
        driver.get(baseUrl);
        
        Addition(driver);
        
        new Actions(driver).moveToElement(new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Input:')]")))).perform();
    	new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='_3ci9dP6l']//span[contains(text(),'Plain Text')]"))).click();
    	
    	
        WebElement Notebook=driver.findElement(By.xpath("//span[contains(text(),'Continue in computable notebook �')]"));
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(Notebook)).click();
        
   
        // 4th point functions:
        WebDriverWait firstplaybutton = new WebDriverWait(driver, 900);
        firstplaybutton.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[3]/div/div[1]/div/div[2]/div/div/div/div/div[4]/div/div[2]/div/div[3]/div/div[7]/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div"))).click();
        
        WebDriverWait secondplaybutton = new WebDriverWait(driver, 800);
        secondplaybutton.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[3]/div/div[1]/div/div[2]/div/div/div/div/div[4]/div/div[2]/div/div[3]/div/div[10]/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div"))).click();
       

        String parentGUID = driver.getWindowHandle();
		// click the button to open new window
        
        WebDriverWait wait1 = new WebDriverWait(driver, 700);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Fast Introduction to the Wolfram Language for Math')]"))).click();
		
		// get the All the session id of the browsers
		Set<String> allGUID = driver.getWindowHandles();
		
		// iterate the values in the set
		for(String guid : allGUID){
			// one enter into if blobk if the GUID is not equal to parent window's GUID
			if(! guid.equals(parentGUID)){
				// switch to the guid
				driver.switchTo().window(guid);
				// break the loop
				break;
			}
			
		}
        
    }
  
  
public static void Addition(WebDriver driver) {
	WebElement Equal= driver.findElement(By.className("_3YyOB_vi"));
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div[1]/section/form/div/div/input")).sendKeys("2+2");
    Equal.click();
	
}
//public static void Hover(WebDriver driver,WebElement element)
//{
	//Actions action= new Actions(driver);
	//new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(element)).click();
	//action.moveToElement(element).click().perform();
	
	//new Actions(driver).moveToElement(new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Input:')]")))).perform();
	//new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='_3ci9dP6l']//span[contains(text(),'Plain Text')]"))).click();
	
//}
public static void HoverAndClick(WebDriver driver,WebElement elementToHover,WebElement elementToClick,WebElement Notebook) {
	Actions action=new Actions(driver);
	action.moveToElement(elementToHover).click(elementToClick).click(Notebook).build().perform();
	
}



}

